# Generate student feedback for exam results. Produces a summary feedback
# that can be added to Moodle's feedback column.
#
# IMPORTANT!! The .dat files we parse here have a very ugly format and we need
# to use hard-coded slicing by column number (i.e., how many characters from
# the start of the line, counting from 0, have the information we want to pull
# out). If the format changes even slightly in the .dat files we get back, this
# script will horribly, horribly fail.  I've created global variables with the
# start and end positions of the data we're pulling out to make editing easier,
# but it's very important to double check this if you start getting gibberish
# in the output (best case scenario) or errors messages out the wazoo (more
# likely).
#
# Robert F. Paul, 2018. Correspondence: robert [dot] f [dot] paul [at] Gmail
# Licensed under GPL 3.0 -- https://www.gnu.org/licenses/gpl-3.0.en.html

#===============================================================================
# Links to relevant documentation and resources

# Python 3 Documentation: https://docs.python.org/3/index.html
# Python 3 Tutorial: https://docs.python.org/3/tutorial/index.html
# pandas Documentation: https://pandas.pydata.org/pandas-docs/stable/
# pandas Tutorial: https://pandas.pydata.org/pandas-docs/stable/getting_started/tutorials.html
# pandas cheat sheet: https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf

# Strings: https://docs.python.org/3/tutorial/introduction.html#strings
#   > String formatting: https://docs.python.org/3/tutorial/inputoutput.html#fancier-output-formatting
# Lists: https://docs.python.org/3/tutorial/introduction.html#lists
#   > List comprehensions: https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions
# Dictionaries: https://docs.python.org/3/tutorial/datastructures.html#dictionaries
# Other built-in types: https://docs.python.org/3/library/stdtypes.html

# Logical comparison: https://docs.python.org/3/tutorial/introduction.html#first-steps-towards-programming
# if statements: https://docs.python.org/3/tutorial/controlflow.html#if-statements
# for statements: https://docs.python.org/3/tutorial/controlflow.html#for-statements
# enumerate, zip, and other looping techniques: https://docs.python.org/3/tutorial/datastructures.html#looping-techniques
# More on conditions and comparisons: https://docs.python.org/3/tutorial/datastructures.html#more-on-conditions

# Functions: https://docs.python.org/3/tutorial/controlflow.html#defining-functions
# Text file input/output: https://docs.python.org/3/tutorial/inputoutput.html#reading-and-writing-files

# pandas basic functionality: https://pandas.pydata.org/pandas-docs/stable/getting_started/basics.html
# pandas input/output: https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html
# pandas indexing: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html
# pandas merging: https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html
# pandas text data operations: https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html

#===============================================================================

# Package imports
import pandas as pd

# Global variables
# Are we using dialogs boxes for the file input/output?
# Otherwise we'll use hard-coded file names.
dialogInteractive = False

# Are we using pop-up dialogs for input/output?
if dialogInteractive:
    # Where are the individual MC scores reports?
    
    # How many points is each MC question worth?
    
    # Where is the file with the exam misconceptions key?
    
    # Where is the Moodle gradebook? CSV format only!
    
    # What is the filepath for the output?
    
    # Adjustments to MC points

    pass
# We're using hard-coded paths/variables for input/output
else:
    # Where are the individual MC scores reports? (DAT files)
    # AL1
    # Exam 1
    # mcPaths = ['/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 1/Exam feedback/AL1 .dat files/jd30707a.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 1/Exam feedback/AL1 .dat files/jd30707b.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 1/Exam feedback/AL1 .dat files/jd30707c.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 1/Exam feedback/AL1 .dat files/jd30707d.dat']
    
    # Exam 2
    # mcPaths = ['/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL1 .dat files/jd31134a-2.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL1 .dat files/jd31134b-2.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL1 .dat files/jd31134c.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL1 .dat files/jd31134d.dat']
    
    # Exam 3
    mcPaths = ['/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 feedback/AL1 .dat files/jd31368b.dat',
    '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 feedback/AL1 .dat files/jd31368a.dat',
    '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 feedback/AL2 .dat files/jd31393a.dat',
    '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 feedback/AL2 .dat files/jd31393b.dat']
    
    # AL2 + AL3
    # mcPaths = ['/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL2 .dat files/jd31142a.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL2 .dat files/jd31142b.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL2 .dat files/jd31142c.dat',
    # '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/AL2 .dat files/jd31142d.dat']
    
    # How many points is each MC question worth?
    # Exam 1
    # mcPoints = 3
    # Exam 2
    # mcPoints = 5
    # Exam 3
    mcPoints = 8
    
    # Where is the file with the exam misconceptions key?
    # CSV format only!
    # Format of the key should be:
    # | Question | A-A | A-B | A-C | A-D | A-E | B-A | B-B | ...
    # | 1        | ... | ... | ... | ... | ... | ... | ... | ...
    
    # Exam 1
    # keyPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 1/Exam feedback/FA19 Exam 1 feedback key.csv"
    
    # Exam 2
    # keyPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Exam 2 Feedback/FA19 Exam 2 feedback key.csv"
    
    # Exam 3
    keyPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 feedback/Exam 3 - exam feedback key (FA19).csv"
    
    # Where is the Moodle gradebook/roster? CSV format only!
    # AL1
    rosterPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Grades/Moodle/Current.csv"
    
    # AL2
    # rosterPath = '/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 1/Exam feedback/AL2 .dat files/IB 150 AL2 FA19 Exam 1 roster.csv'
    
    # AL3
    # rosterPath = '/Users/exnihilo/Box_Sync/IB 150 Spring 2019/IB 150 AL1 SP19/Exam files/IB 150 AL3 SP19 roster.csv'
    
    # What is the filepath for the output? CSV format only!
    # AL1
    # Exam 1
    # outPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 1/Feedback Output/AL1/AL1 - Exam 1 response feedback.csv"
    
    # Exam 2
    # outPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 2/Feedback Output/AL1/AL1 - Exam 2 response feedback.csv"
    
    # Exam 3
    outPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 feedback/AL1/AL1 - Exam 3 response feedback.csv"
    
    # AL2
    # outPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 feedback/AL2/AL2 - Exam 3 response feedback.csv"
    
    # AL3
    # outPath = "/Users/exnihilo/Box_Sync/IB 150 Spring 2019/IB 150 AL1 SP19/Exam files/Exam 3/Feedback_Output/AL3/AL3 - Exam 3 response feedback.csv"
    
    # Adjustments to MC points
    adjustment = 0
    
    # NetID (ID only, NOT the email address) of student response to pull aside
    # for additional checks, e.g, possible wrong form, regrade requests, etc.
    checkID = ""

# Start index of last name entry
nameStart = 40

# End index of last name entry (remember that slice endpoint is non-inclusive!)
nameEnd = 50

# Start index of NetID entry
netIDStart = 63

# End index of NetID entry (remember that slice endpoint is non-inclusive!)
netIDEnd = 71

# Index of exam form
formIndex = 71

# Start index of MC responses
responseStart = 72

# End index of MC responses (remember that slice endpoint is non-inclusive!)
responseEnd = 97

# Function definitions
# Get the answer key and the form type
def ParseAnswerKey(datLine):
    # Parse out the answer key
    mcKey = datLine[responseStart:responseEnd]
    # Turn the string into a list of strings
    mcKey = [i for i in mcKey]
    # Get the form letter
    formLetter = datLine[formIndex]
    
    return mcKey, formLetter

# Get the current student's last name, NetID, and responses (as a list of values)
def ParseStudent(datLine, form):
    # Get the student's last name; strip whitespace
    name = datLine[nameStart:nameEnd].strip()
    # Get the student's NetID; strip any whitespace and use lowercase letters
    netID = datLine[netIDStart:netIDEnd].strip().lower()
    # Get the form type
    studentForm = datLine[formIndex]
    # Warn if the forms mismatch (usually when student leaves the form blank)
    if (form != studentForm):
        print("Warning! Form mismatch for {}: bubbled-in".format(netID) + 
            " form is reportedly [ {} ], test stack is {}".format(studentForm,
                form))
    # Parse out the responses
    responses = datLine[responseStart:responseEnd]
    # Turn the responses into a list of strings
    responses = [i for i in responses]
    
    return name, netID, responses

# Create the text feedback based on keyed-out response. Note that for line
# breaks we use the canonical HTML tag of <br> and enclose the entire cell in a 
# paragraph tag, <p> </p>
def GenerateFeedback(resp, form):
    # Misconceptions identified
    misconceptions = CollectFeedback(resp, form)
    # Number of correct responses
    numRight = sum([i == j for i, j in zip(mcKey, responses)])
    
    # Determine their total points earned for the MC section
    feedback = "<p>Total points earned from multiple choice: {}/{}<br><br>".format(
        (numRight * mcPoints) + adjustment,
        len(responseKey) * mcPoints)
    
    # Add text breakdown to the feedback.
    feedback = feedback + "Misconceptions identified from your exam responses:<br><font size=1>"
    
    # **AL2, alternative without points
    # Add text breakdown to the feedback.
    # feedback = "<p>Feedback on missed exam questions:<br><font size=1>"
    
    # Go through all the misconceptions identified
    for miss in misconceptions:
        # Add the misconception to the feedback. #10146 is a triangular bullet
        # character.
        feedback = feedback + "&#10148; " + miss + "<br>"
    
    # Close the HTML tags
    feedback = feedback + "</font></p>"

    return feedback

# Return the feedback for each wrong answer as a list of strings
def CollectFeedback(resp, form):
    # Dictionary relating ordinal numeric response to bubbled-in letter response
    ordinalsToLetters = {'1':'A',
                         '2':'B',
                         '3':'C',
                         '4':'D',
                         '5':'E',
                         # Answer left blank
                         ' ':'X'}
    
    # The exam form and letters of the responses; used as a column index when
    # looking up the feedback. This creates a list like ["A-E", "A-A", "A-C"...]
    feedbackColumns = [form + '-' + ordinalsToLetters[i] for i in resp]

    # Enumerate (count up) the responses, so we now have a list of lists like
    # [ [0, "A-E"], [1, "A-A"], [2, "A-C"]...]
    validColumns = [[i, j] for i, j 
                    in enumerate(feedbackColumns)
                    # But ignore blank responses--we can't give feedback without
                    # a response!
                    if 'X' not in j]
    
    # Look up each response in the in the response key by row (question number)
    # and answer given...
    misconceptions = [responseKey[j][i] for i, j
                      in validColumns
                      # but only if it's NOT correct (the correct answers in the
                      # key are blank cells, represented as 'nan')
                      if str(responseKey[j][i]) != 'nan']
    # Remove any duplicate misconceptions
    misconceptions = list(set(misconceptions))
    # Sort the list
    misconceptions.sort()
    
    return misconceptions

# Main script run
# Create an empty list to store the summary data
buildSummary = []

# Load the misconceptions key
responseKey = pd.read_csv(keyPath, dtype = str)

# For each file...
for path in mcPaths:
    # Open the file in read mode
    with open(path, mode='r') as mcFile:
        # Get the answer key from the first line
        firstLine = mcFile.readline()
        mcKey, mcForm = ParseAnswerKey(firstLine)
        print("\tForm {} key: {}".format(mcForm, mcKey))
        
        # For each student...
        for line in mcFile:
            # Ignore that weird end-of-file character in the files...
            if (len(line) > 1):
                # Get the information about this student
                name, netID, responses = ParseStudent(line, mcForm)
                
                # Is this a response we're pulling out for additional checks?
                if netID == checkID:
                    # If so, copy their name and responses
                    checkName = name
                    checkResp = responses
                
                # Add the student's entry to the summary. The order of columns
                # here is Last name, NetID, email, and feedback
                buildSummary.append(
                    [name,
                    netID,
                    netID + "@illinois.edu",
                    GenerateFeedback(responses, mcForm)])

# Make a dataframe out of the list of student information and feedback
summary = pd.DataFrame(buildSummary,
                       columns = ["Last name",
                                  "NetID",
                                  "Email address",
                                  "Feedback"])

# Get the email addresses and last names from the current Moodle roster
roster = pd.read_csv(rosterPath)
roster = roster[["Email address", "Last name"]]
# Rename "Last name" from roster to "Last name (Moodle)"
roster = roster.rename(
    index = str, columns = {"Last name" : "Last name (Moodle)"})

# Merge the current Moodle roster with the feedback
merged = roster.merge(
    summary,
    on = "Email address",
    how = 'outer',
    indicator = "Found in Moodle or MC")

# Sort by Last name (from Moodle) after sorting by Last name (from MC).
merged = merged.sort_values(by = ["Last name (Moodle)", "Last name"])
# Get the indices of drops or incorrect NetID entries
mismatchIndices = merged["Found in Moodle or MC"] == "right_only"
# Copy the mismatches into a new dataframe
mismatches = merged[mismatchIndices]
# Drop the mismatches from the main dataframe
merged = merged[~mismatchIndices]

# Get the indices of NetID entries for conflicts/makeups/DRES/etc.
blankIndices = merged["Found in Moodle or MC"] == "left_only"
# Copy the mismatches into a new dataframe
blanks = merged[blankIndices]
# Drop the mismatches from the main dataframe
merged = merged[~blankIndices]

# Drop the other blank entries (e.g. from dropped/conflict students)
merged = merged[merged["Found in Moodle or MC"] == "both"]

# Create the filename for the mismatch file
mismatchPath = outPath[:-4] + "--email_mismatches.csv"

# Create the filename for the blanks file
blanksPath = outPath[:-4] + "--blanks.csv"

## Write to output. Overwrites by default!
# Write the mismatched ID summary report output
mismatches.to_csv(mismatchPath, index = False)
# Write the blanks summary report
blanks.to_csv(blanksPath, index = False)
# Write the summary report output
merged.to_csv(outPath, index = False)
