# Align multiple choice and prepare the gradesheet for grade entry
# Robert F. Paul, 2018. Correspondence: robert [dot] f [dot] paul [at] Gmail
# Licensed under GPL 3.0 -- https://www.gnu.org/licenses/gpl-3.0.en.html

#===============================================================================
# Links to relevant documentation and resources

# Python 3 Documentation: https://docs.python.org/3/index.html
# Python 3 Tutorial: https://docs.python.org/3/tutorial/index.html
# pandas Documentation: https://pandas.pydata.org/pandas-docs/stable/
# pandas Tutorial: https://pandas.pydata.org/pandas-docs/stable/getting_started/tutorials.html
# pandas cheat sheet: https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf

# Strings: https://docs.python.org/3/tutorial/introduction.html#strings
# Lists: https://docs.python.org/3/tutorial/introduction.html#lists
#   > List comprehensions: https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions
# Dictionaries: https://docs.python.org/3/tutorial/datastructures.html#dictionaries
# Other built-in types: https://docs.python.org/3/library/stdtypes.html

# Logical comparison: https://docs.python.org/3/tutorial/introduction.html#first-steps-towards-programming
# if statements: https://docs.python.org/3/tutorial/controlflow.html#if-statements
# for statements: https://docs.python.org/3/tutorial/controlflow.html#for-statements
# More on conditions and comparisons: https://docs.python.org/3/tutorial/datastructures.html#more-on-conditions

# Functions: https://docs.python.org/3/tutorial/controlflow.html#defining-functions

# pandas basic functionality: https://pandas.pydata.org/pandas-docs/stable/getting_started/basics.html
# pandas input/output: https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html
# pandas indexing: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html
# pandas merging: https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html
# pandas text data operations: https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html

#===============================================================================

# Package imports
import pandas as pd

# Are we using dialog boxes for defining file paths? 
dialogInteractive = False

if dialogInteractive:
    pass
else:
    # Where is the DMI Excel roster?
    dmiPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Rosters/Roster.xls"
    
    # Where is the lookup table for TA sections?
    sectionPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Rosters/Section-TA_table.csv"
    
    # Where is the multiple choice data?
    mcPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/sf31368.csv"
    
    # Do we have free response grades already started?
    partialGrading = False
    
    if partialGrading:
        # Where are the free response scores? IMPORTANT: these need to match the
        # format of the aligned output exactly! Refer to template/example file
        partialPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Exam 3 - In progress.csv"
    
    # Where are we saving the aligned output data?
    outPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Exam Files/Exam 3/Merged_aligned_output.csv"

# Function definitions
# Retreive roster information from the DMI Roster tool at
# https://secure.dmi.illinois.edu/sil/roster.asp
# We need to use this until Moodle can export section information!
def GetRosterDMI(rPath):
    # Get the roster. DMI's Excel format is weird--it's actually an HTML file
    # The header is at line 12
    # N.B., read_html gets a list of dataframes, our data should be at index 0
    df = pd.read_html(rPath, header = 12)[0]
    # Only include currently registered students
    df = df.loc[df["Registration Status"].isin(['RW', 'RE'])]
    # Drop everything except section, email, and name
    df = df[["Section", "Email", "Student Name"]]
    # Split "Last, First M" format into separate columns for first and last name
    df[["Last name", "First name"]] = df["Student Name"].str.split(", ", expand = True)
    # Drop the middle initial from "First name" if it's present
    # N.B., a student with a compound first name like "Mary Ann" will have the
    # "Ann" dropped; if that's an issue, redo this line so that only a trailing
    # single character is dropped. A well-constructed regular expression is
    # probably the most parsimonious solution, an if statement is probably the
    # most readable.
    df["First name"] = df["First name"].str.split().str.get(0)
    
    # Drop "Student Name" column and rearrange the other columns
    df = df[["Section", "Email", "First name", "Last name"]]
    return df

# What should the items and order of the columns be?
colsOut = ['Last name-MC',
           'Initial',
           'UIN',
           'Numeric section',
           'Form',
           'User Id',
           'Email',
           'MC+DMI',
           'First name',
           'Last name',
           'Section',
           'TA',
           'MC',
           'SA1',
           'SA1 Grader',
           'SA2',
           'SA2 Grader',
           'SA3',
           'SA3 Grader',
           'E',
           'E Grader']

# Load the multiple choice scores
mcScores = pd.read_csv(mcPath)
# Rename the MC columns
mcScores = mcScores.rename(columns = {"Section": "Numeric section",
                                      "Last Name": "Last name-MC",
                                      "Score 1": "MC"})
# Create an "Email" column
mcScores["Email"] = mcScores["User Id"] + "@illinois.edu"

# Load the DMI roster
rosterData = GetRosterDMI(dmiPath)

# Load the lookup table for which sections the TAs teach
sectionLookup = pd.read_csv(sectionPath)

# Merge the data with the roster
merged = mcScores.merge(rosterData, how="outer", on="Email", indicator = "MC+DMI")

# Add the TA information to the data
merged = merged.merge(sectionLookup, how = "outer", on="Section")

# Do we have some free response scores already recorded?
if partialGrading:
    # Load the grade sheet where free responses have already been recorded
    currentGrading = pd.read_csv(partialPath)
    # Set the indices of the dataframes to email addresses
    merged.set_index("Email", inplace = True)
    currentGrading.set_index("Email", inplace = True)
    # Update the current grade sheet with the MC data
    currentGrading.update(merged)
    # Reset the indices
    currentGrading.reset_index(inplace = True)
    merged.reset_index(inplace = True)
    # Replace the roster-merged MC data with the updated free response data
    # We also concatenate the NetID mismatches/typos to the end
    merged = pd.concat(
        [currentGrading, merged[merged["MC+DMI"]=="left_only"]],
        sort = False)
    # Drop duplicate rows
    merged.drop_duplicates(inplace = True)

# Change the column order to what's in colsOut
merged = merged.reindex(columns = colsOut)

# Sort by TA, Last name
merged.sort_values(by = ["TA", "Last name"], inplace = True)

## Write results
# Write to CSV, OVERWRITES BY DEFAULT!
merged.to_csv(outPath, index = False)
