# Merges data from the current gradesheet, iClicker, and discussion grades to
# an upload master gradesheet for updating the Mooodle gradebook
# Robert F. Paul, 2018. Correspondence: robert [dot] f [dot] paul [at] Gmail
# Licensed under GPL 3.0 -- https://www.gnu.org/licenses/gpl-3.0.en.html

#===============================================================================
# Links to relevant documentation and resources

# Python 3 Documentation: https://docs.python.org/3/index.html
# Python 3 Tutorial: https://docs.python.org/3/tutorial/index.html
# pandas Documentation: https://pandas.pydata.org/pandas-docs/stable/
# pandas Tutorial: https://pandas.pydata.org/pandas-docs/stable/getting_started/tutorials.html
# pandas cheat sheet: https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf

# Strings: https://docs.python.org/3/tutorial/introduction.html#strings
# Lists: https://docs.python.org/3/tutorial/introduction.html#lists
#   > List comprehensions: https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions
# Other built-in types: https://docs.python.org/3/library/stdtypes.html

# Logical comparison: https://docs.python.org/3/tutorial/introduction.html#first-steps-towards-programming
# if statements: https://docs.python.org/3/tutorial/controlflow.html#if-statements
# for statements: https://docs.python.org/3/tutorial/controlflow.html#for-statements
# More on conditions and comparisons: https://docs.python.org/3/tutorial/datastructures.html#more-on-conditions

# Functions: https://docs.python.org/3/tutorial/controlflow.html#defining-functions

# pandas basic functionality: https://pandas.pydata.org/pandas-docs/stable/getting_started/basics.html
# pandas input/output: https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html
# pandas indexing: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html
# pandas merging: https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html
# pandas text data operations: https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html

#===============================================================================

# Package imports
import pandas as pd
import xlsxwriter

# Global variables
# Are we using dialogs boxes for the file input/output and other settings?
# Otherwise we'll use hard-coded file names.
dialogInteractive = False

if dialogInteractive: #TODO
    # Where is our exported Moodle grade file? (CSV only!)
    
    # Where is the iClicker manual override file?
    
    # Where are the discussion grade Excel files?
    
    # And where are we saving the resulting grade import file we're uploading?
    
    # Which columns are we keeping for the upload master update?
    
    # iClicker participation score
    
    # Any rescaling?
    
        # If so, what's the item and the rescale value?

    pass
else:
    # Where is our exported Moodle grade file? (CSV only!)
    exportFile = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Grades/Moodle/Current.csv"
    # Where is the iClicker manual override file?
    # iClickerFile = "/Users/exnihilo/Box_Sync/IB 150 Spring 2019/IB 150 AL1 SP19/Grades/iClicker_credit_tracking.csv"
    
    # Where are the discussion grade Excel files?
    discussionFiles = ["/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Ben - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Christian - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Duncan - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Ed - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Hannah - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Kenny - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Maggie - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Robert - IB150 AL1 FA19.xlsx",
    "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Sarai - IB150 AL1 FA19.xlsx"]
    
    # And where are we saving the resulting grade import file we're uploading?
    uploadFile = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Grades/Upload_Master.csv"
    
    # Which columns are we keeping for the upload master update?
    keepCols = ["Email address",
                "Week 2 Discussion (Real)",
                "Week 3 Discussion (Real)",
                "Week 4 Discussion (Real)",
                "Week 5 Discussion (Real)",
                "Week 6 Discussion (Real)",
                "Week 7 Discussion (Real)",
                "Week 8 Discussion (Real)",
                "Week 9 Discussion (Real)",
                "Week 10 Discussion (Real)",
                "Week 11 Discussion (Real)",
                "Week 12 Discussion (Real)",
                "Week 13 Discussion (Real)",
                "Week 14 Discussion (Real)",
                "Assignment:Getting to Know My Classmates (Grade) (Real)"]

    # iClicker participation score
    # iClickerScore = 4.0
    
    # Any rescaling?
    # needsRescale = True
    
    # If so, what's the item and the rescale value?
    # if (needsRescale):
    #     rescaleItem = "Week 15 Discussion (Real)"
    #     rescaleValue = 0.8

# Load the current data exported from Moodle
currData = pd.read_csv(exportFile)

# Load the manual iClicker data entry file
# Deprecated; replaced with drop policy
# Expected structure of the file:
# Name          Email Address         Manual entry 1     ... Absence credit n
# [anything]    netid@illinois.edu    Lecture x.x (Real) ... Lecture x.x (Real)
# Empty data: Leave it blank OR write something that doesn't match a grade
# column (e.g., this is useful if you need to defer a grade for pending
# documentation, so you can enter "Defer - Lecture 2.1 (Real)" and then remove
# the "Defer - " part once you can excuse the absence
# iData = pd.read_csv(iClickerFile)

# Drop everything but the columns we need
currData = currData[keepCols]

# Update the upload file from each file of discussion grades
for discPath in discussionFiles:
    # Open the discussion grade
    currDisc = pd.read_excel(discPath)
    # Set email address as the discussion grade index
    currDisc.set_index('Email', inplace = True)
    # Now for our upload file, change the index to email address...
    currData.set_index('Email address', inplace = True)
    # Update it...
    currData.update(currDisc)
    # Then reset the indexing back to what it was
    currData.reset_index(inplace = True)

# Update the upload file with the manual submission/absence iClicker scores
# Go by each email address through the iClicker list
# Deprecated due to drop policy
# for email in iData['Email address']:
#     # Unpack the values in the current student's row as a list
#     unpacked = iData.loc[
#         iData['Email address'] == email].values.flatten().tolist()
#     # Find which sessions have full credit given to this user
#     sessions = [x for x in unpacked if str(x).find("Lecture") >= 0]
#     # Only use the sessions defined in keepCols
#     sessions = [x for x in sessions if x in keepCols]
#     
#     for session in sessions:
#         # Handle key errors
#         try:
#             # Use email address and lecture to enter override value 
#             currData.loc[
#                 currData['Email address'] == email, session] = iClickerScore
#         # Entry update failed, output a message
#         except:
#             print("Failed to override {} for {}".format(session, email))

# Sort results by email address
currData.sort_values(by = 'Email address', inplace = True)  

# Warn about transfer student email addresses--these will NOT upload to Moodle!
transferEmails = currData[~ currData["Email address"].str.contains("@illinois.edu")]
if len(transferEmails) > 0:
    print("Transfer students found! These students DO NOT consistently have " +
        "@illinois.edu email addresses and likely won't have their grades " +
        "updated properly! Please contact these students directly:")
    print(transferEmails['Email address'].to_string())

# Get the columns that have only score data
scoreCols = [col for col in currData.columns if col != "Email address"]
# Convert anything that isn't a number into NaN
currData[scoreCols] = currData[scoreCols].apply(pd.to_numeric,
    errors = 'coerce')

# Do we need to rescale anything?
# if (needsRescale):
#     currData[rescaleItem] = currData[rescaleItem] * rescaleValue

## Save the upload file; overwrites by default!
currData.to_csv(uploadFile, index = False)
