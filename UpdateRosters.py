# Update discussion gradesheet rosters using the DMI Excel roster from
# https://secure.dmi.illinois.edu/sil/roster.asp
# IMPORTANT: The head TA is not given access to this roster automatically;
# contact the SIB admin office to set it up
# Robert F. Paul, 2018. Correspondence: robert [dot] f [dot] paul [at] Gmail
# Licensed under GPL 3.0 -- https://www.gnu.org/licenses/gpl-3.0.en.html

#===============================================================================
# Links to relevant documentation and resources

# Python 3 Documentation: https://docs.python.org/3/index.html
# Python 3 Tutorial: https://docs.python.org/3/tutorial/index.html
# pandas Documentation: https://pandas.pydata.org/pandas-docs/stable/
# pandas Tutorial: https://pandas.pydata.org/pandas-docs/stable/getting_started/tutorials.html
# pandas Cheat Sheet: https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf
# XlsxWriter Documentation: https://xlsxwriter.readthedocs.io/

# Strings: https://docs.python.org/3/tutorial/introduction.html#strings
#   > String formatting: https://docs.python.org/3/tutorial/inputoutput.html#fancier-output-formatting
# Lists: https://docs.python.org/3/tutorial/introduction.html#lists
#   > List comprehensions: https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions
# Tuples: https://docs.python.org/3/tutorial/datastructures.html#tuples-and-sequences
# Other built-in types: https://docs.python.org/3/library/stdtypes.html

# Logical comparison: https://docs.python.org/3/tutorial/introduction.html#first-steps-towards-programming
# if statements: https://docs.python.org/3/tutorial/controlflow.html#if-statements
# for statements: https://docs.python.org/3/tutorial/controlflow.html#for-statements
# enumerate, zip, and other looping techniques: https://docs.python.org/3/tutorial/datastructures.html#looping-techniques
# More on conditions and comparisons: https://docs.python.org/3/tutorial/datastructures.html#more-on-conditions
# Iteration with next: https://docs.python.org/3/tutorial/classes.html#iterators

# Functions: https://docs.python.org/3/tutorial/controlflow.html#defining-functions
# Error handling: https:docs.python.org/3/tutorial/errors.html#handling-exceptions
#   > The sys module: https://docs.python.org/3/tutorial/modules.html#standard-modules

# pandas basic functionality: https://pandas.pydata.org/pandas-docs/stable/getting_started/basics.html
# pandas input/output: https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html
# pandas indexing: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html
# pandas merging: https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html
# pandas text data operations: https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html

# XlsxWriter Tutorials
#   > Create a simple XLSX file: https://xlsxwriter.readthedocs.io/tutorial01.html
#   > Adding formatting to the XLSX File: https://xlsxwriter.readthedocs.io/tutorial02.html
#   > Writing different types of data to the XLSX File: https://xlsxwriter.readthedocs.io/tutorial03.html
# pandas with XlsxWriter examples: https://xlsxwriter.readthedocs.io/pandas_examples.html
# Cell notation/referencing: https://xlsxwriter.readthedocs.io/working_with_cell_notation.html
#   > User-defined header formatting: https://xlsxwriter.readthedocs.io/example_pandas_header_format.html

#===============================================================================

# Package imports
import sys
import pandas as pd
import xlsxwriter

# Global variables

# Are we using dialogs boxes for the file input/output?
# Otherwise we'll use hard-coded file names.
dialogInteractive = False

if dialogInteractive: #TODO
    # Are we starting a new roster file?
    
    # Where is the roster file?
    
    # Where are the discussion grade Excel files?
    
    # What is the sorting by?
    
    # Where is the drops file?
    
    # What are the names of the columns?

    pass
else:
    # Are we starting a new roster file?
    initializeFiles = False
    
    # If we're starting a new roster file, we'll need to get the sections
    # associated with each TA. Where is the Section-TA lookup table?
    if initializeFiles:
        sectionPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Rosters/Section-TA_table.csv"
    
    # Where is the roster file?
    rosterPath = "/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/Rosters/Roster.xls"
    
    # Where are the discussion grade Excel files? What is the sorting by?
    # Data structure is a list of tuples like this:
    # [ (filepath1, sortingColumn1), (filepath2, sortingColumn2)...]
    discussionFiles = [
        #("/Users/exnihilo/Box_Sync/IB 150 FA19/IB 150 AL1 FA19/IB 150 AL1 FA19 - TA Resources/Discussion Grades/Ben - IB150 AL1 FA19.xlsx", "Last name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Christian - IB150 AL1 FA19.xlsx", "Last name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Duncan - IB150 AL1 FA19.xlsx", "Last name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Ed - IB150 AL1 FA19.xlsx", "Last name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Hannah - IB150 AL1 FA19.xlsx", "First name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Kenny - IB150 AL1 FA19.xlsx", "Last name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Maggie - IB150 AL1 FA19.xlsx", "Last name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Robert - IB150 AL1 FA19.xlsx", "First name"),
        ("/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Sarai - IB150 AL1 FA19.xlsx", "Last name")]

    # Where is the drops file?
    dropPath = "/Users/exnihilo/Box_Sync/IB 150 - Scratch Folder/Sample Data - Working Folder/Generate Roster/Drops.xlsx"
    
    # What are the names of the columns?
    colNames = ["Section", "Email", "First name", "Last name"]

# Function definitions
# Retreive roster information from the DMI Roster tool at
# https://secure.dmi.illinois.edu/sil/roster.asp
def GetRosterDMI(rPath):
    # Get the roster. DMI's Excel format is weird--it's actually an HTML file
    # The header is at line 12
    # N.B., read_html gets a list of dataframes, our data should be at index 0
    df = pd.read_html(rPath, header = 12)[0]
    # Only include currently registered students
    df = df.loc[df["Registration Status"].isin(['RW', 'RE'])]
    # Drop everything except section, email, and name
    df = df[["Section", "Email", "Student Name"]]
    # Split "Last, First M" format into separate columns for first and last name
    df[["Last name", "First name"]] = df["Student Name"].str.split(", ", expand = True)
    # Drop the middle initial from "First name" if it's present
    # N.B., a student with a compound first name like "Mary Ann" will have the
    # "Ann" dropped; if that's an issue, redo this line so that only a trailing
    # single character is dropped. A well-constructed regular expression is
    # probably the most parsimonious solution, an if statement is probably the
    # most readable.
    df["First name"] = df["First name"].str.split().str.get(0)
    
    # Drop "Student Name" column and rearrange the other columns
    df = df[["Section", "Email", "First name", "Last name"]]
    return df

# Export data to a formatted Excel file
def ExportData(df, path):
    # Initialize Excel Writer
    writer = pd.ExcelWriter(path, engine = 'xlsxwriter')
    # Export the dataframe to the Excel writer
    # Header is empty to allow custom formatting
    df.to_excel(writer,
                index = False,
                header = False,
                startrow = 1,
                freeze_panes = (1, 5),
                engine = 'xlsxwriter')
    # Initialize the workbook object
    workbook = writer.book
    # Initialize the worksheet object
    worksheet = writer.sheets['Sheet1']
    
    # Set the cell format
    format = workbook.add_format({'font_size': 12,
        'font_name': 'Helvetica'})
    # Set the header format
    headerFormat = workbook.add_format({'font_size': 12,
        'font_name': 'Helvetica',
        'bold': True,
        'bg_color': '#CCCCCC'}) # Light gray background color
    # Set the widths and formatting for the columns and header
    worksheet.set_column('A:B', 8, format) # TA, Section
    worksheet.set_column('C:C', 25, format) # Email
    worksheet.set_column('D:D', 12, format) # First
    worksheet.set_column('E:E', 20, format) # Last
    worksheet.set_column('F:Z', 9, format) # Scores
    
    # Write the formatted column headers
    for col_num, value in enumerate(df.columns.values):
        worksheet.write(0, col_num, value, headerFormat)
    
    # Save the file
    writer.save()

## Initialize or load files
# Get the DMI roster data loaded into a dataframe
rosterData = GetRosterDMI(rosterPath)

# Are we initializing new roster files?
if initializeFiles:
    # Get the TA-to-section lookup table loaded
    sectionLookup = pd.read_csv(sectionPath)
    
    # Set up the new headers
    newHeaders = ["TA"] + colNames + ["Week 2 Discussion (Real)",
        "Week 3 Discussion (Real)",
        "Week 4 Discussion (Real)",
        "Week 5 Discussion (Real)",
        "Week 6 Discussion (Real)",
        "Week 7 Discussion (Real)",
        "Week 8 Discussion (Real)",
        "Week 9 Discussion (Real)",
        "Week 10 Discussion (Real)",
        "Week 11 Discussion (Real)",
        "Week 12 Discussion (Real)",
        "Week 13 Discussion (Real)",
        "Week 14 Discussion (Real)"]
    
    # Create an empty dataframe to use as a template
    dfTemplate = pd.DataFrame(columns = newHeaders)
    
    # Export the empty template as the initialized Drops file
    ExportData(dfTemplate, dropPath)
    print("Initialized an empty drops file at {}".format(dropPath))
    
    # For each discussion file...
    for discPath, sortCol in discussionFiles:
        # This might fail, especially if the file paths or the TA table have
        # typos; so we put this in a try block
        try:
            # Get the TA name by cross-referencing file name and TA lookup table
            currTA = next((x for x in sectionLookup['TA'] if x in discPath))
            # Copy the TA and section data into a new dataframe
            currDisc = sectionLookup[sectionLookup["TA"] == currTA]
            # Merge the data frames on Section data
            merged = currDisc.merge(rosterData,
                how = 'left',
                on = 'Section',
                indicator = False)
            # Add the new columns to the newly merged dataframe
            merged = merged.reindex(columns = newHeaders)
            # Save the dataframe as a new file. Overwrites by default!
            ExportData(merged, discPath)
            print("Initialized {}".format(discPath))
        
        # Initialization failed for some reason; catch and warn about errors
        except OSError as err:
            print("\n***Failed to initialize {}!".format(discPath))
            print("OS error: {0}".format(err), end="\n\n")
        except:
            print("\n***Failed to initialize {}!".format(discPath))
            print("Unexpected error:", sys.exc_info()[0], end="\n\n")

dropData = pd.read_excel(dropPath)

## Write the updated files
# Update all the discussion files
for discPath, sortCol in discussionFiles:
    # Get the current discussion grade sheet
    currDisc = pd.read_excel(discPath)
    # Which sections does this sheet cover?
    sectionList = list(pd.unique(currDisc["Section"]))
    # Get the current roster for these sections
    subRoster = rosterData.loc[rosterData["Section"].isin(sectionList)]
    # Recast data type for current discussion sheet; newly initialized files
    # will read in as Float64 on blank (NaN) columns and the merge will fail due
    # to data type mismatching on the merged columns. 
    currDisc = currDisc.astype('object')
    # Merge the data frames and indicate where the data came from
    merged = currDisc.merge(subRoster,
                            how = 'outer',
                            on = list(subRoster.columns),
                            indicator = True)
    # Indices of dropped students (as a Series of True/False booleans)
    dropped = merged["_merge"] == "left_only"
    # Append dropped students to Drops data
    dropData = dropData.append(
        merged[dropped].drop(columns = ["_merge"]), sort = False)
    # Removed the dropped students from merged data
    merged = merged[~dropped]
    # Fill in the TA information for added students
    merged.loc[merged["_merge"] == "right_only", "TA"] = merged["TA"][1]
    # Drop the _merge indicator column now that we're done with it
    merged = merged.drop(columns = ["_merge"])
    # Sort according to preferences
    merged.sort_values(by = ["Section", sortCol], inplace = True)
    # Uncomment when testing output
    # discPath = '/Users/exnihilo/tmp/testOut.xlsx'
    # Save the result
    ExportData(merged, discPath)

# Save the drops file
ExportData(dropData, dropPath)
